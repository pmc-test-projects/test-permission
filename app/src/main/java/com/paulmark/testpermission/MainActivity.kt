package com.paulmark.testpermission

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView


class MainActivity : Activity() {

    val requestCodeFine = 1000
    val requestCodeCoarse = 1001
    var permissionFine = 0
    var permissionCoarse = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("log", "onCreate()")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btPermission = findViewById<Button>(R.id.btPermission)
        btPermission.setOnClickListener(View.OnClickListener {
            Log.d("log", "Fine Location Requested")
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestCodeFine)
        })

        val btPermissionCoarse = findViewById<Button>(R.id.btPermissionCoarse)
        btPermissionCoarse.setOnClickListener(View.OnClickListener {
            Log.d("log", "Coarse Location Requested")
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), requestCodeCoarse)
        })
    }

    override fun onResume() {
        Log.d("log", "onResume()")
        super.onResume()
        updateStatus()
    }

    fun updateStatus() {
        permissionFine = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        permissionCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)

        val tvPermission = findViewById<TextView>(R.id.tvPermission)
        if (permissionFine == PackageManager.PERMISSION_GRANTED) {
            tvPermission.setText("Permission Fine Location: GRANTED")
        } else if (permissionFine == PackageManager.PERMISSION_DENIED) {
            tvPermission.setText("Permission Fine Location: DENIED")
        } else {
            tvPermission.setText("Permission Fine Location: ERROR")
        }

        val tvPermissionCoarse = findViewById<TextView>(R.id.tvPermissionCoarse)
        if (permissionCoarse == PackageManager.PERMISSION_GRANTED) {
            tvPermissionCoarse.setText("Permission Coarse Location: GRANTED")
        } else if (permissionCoarse == PackageManager.PERMISSION_DENIED) {
            tvPermissionCoarse.setText("Permission Coarse Location: DENIED")
        } else {
            tvPermissionCoarse.setText("Permission Coarse Location: ERROR")
        }

        val tvPermissionRationale = findViewById<TextView>(R.id.tvPermissionRationale)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            tvPermissionRationale.setText("Rationale Fine: TRUE")
        } else {
            tvPermissionRationale.setText("Rationale Fine: FALSE")
        }

        val tvPermissionCoarseRationale = findViewById<TextView>(R.id.tvPermissionCoarseRationale)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            tvPermissionCoarseRationale.setText("Rationale Coarse: TRUE")
        } else {
            tvPermissionCoarseRationale.setText("Rationale Coarse: FALSE")
        }

        val tvProviderGPS = findViewById<TextView>(R.id.tvProviderGPS)
        val tvProviderNetwork = findViewById<TextView>(R.id.tvProviderNetwork)
        val tvProviderPassive = findViewById<TextView>(R.id.tvProviderPassive)

        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (locationManager != null) {

            tvProviderGPS.visibility = View.VISIBLE
            tvProviderNetwork.visibility = View.VISIBLE
            tvProviderPassive.visibility = View.VISIBLE

            try {
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    tvProviderGPS.setText("Provider GPS: ENABLED")
                } else {
                    tvProviderGPS.setText("Provider GPS: DISABLED")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                tvProviderGPS.setText("Provider GPS: ERROR: " + e.message)
            }

            try {
                if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    tvProviderNetwork.setText("Provider Network: ENABLED")
                } else {
                    tvProviderNetwork.setText("Provider Network: DISABLED")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                tvProviderNetwork.setText("Provider Network: ERROR: " + e.message)
            }

            try {
                if (locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER)) {
                    tvProviderPassive.setText("Provider Passive: ENABLED")
                } else {
                    tvProviderPassive.setText("Provider Passive: DISABLED")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                tvProviderPassive.setText("Provider Passive: ERROR: " + e.message)
            }

        } else {
            tvProviderGPS.visibility = View.GONE
            tvProviderNetwork.visibility = View.GONE
            tvProviderPassive.visibility = View.GONE
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>?, grantResults: IntArray?) {
        updateStatus()
    }
}
